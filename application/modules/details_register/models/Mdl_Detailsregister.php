<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_Detailsregister extends CI_Model
{

function __construct() {
parent::__construct();
}

public function save_detailsregister($detailsregister)
{
	$this->db->insert('detailsregister',['name'=>$detailsregister['name'],'age'=>$detailsregister['age'],'location'=>$detailsregister['location'],'email'=>$detailsregister['useremail'],'contact'=>$detailsregister['contact'],'complications_msg'=>$detailsregister['usermessage'],'date'=>time()]);
}

}
