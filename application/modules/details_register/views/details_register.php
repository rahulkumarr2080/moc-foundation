<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Details for register</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li class="active">Details for register</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column--->
            <div class="form-column offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 ">
                <div class="inner">
                    <div class="default-form contact-form">
                        <form method="post" action="<?= base_url(); ?>details_register/submit" id="registerdetail-form">
                            <?php if ($this->session->flashdata('flash_message')): ?>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                            <?php endif; ?>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="name">Name:</label></div>
                                    <input type="text" id="name" name="name" placeholder="Enter Your Name" value="<?php echo set_value('name'); ?>">
                                    <?php echo form_error('name', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="age">Age:</label></div>
                                    <input type="text" id="age" name="age" placeholder="Enter Your Age" value="<?php echo set_value('age'); ?>">
                                    <?php echo form_error('age', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="location">Location:</label></div>
                                    <input maxlength="20" type="text" id="location" name="location" placeholder="Enter Your Location ( Maximum 20 character )" value="<?php echo set_value('location'); ?>">
                                    <?php echo form_error('location', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="contact">Contact No:</label></div>
                                    <input type="text" id="contact" name="contact" placeholder="Enter Your Contact Number" value="<?php echo set_value('contact'); ?>">
                                    <?php echo form_error('contact', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="useremail">Email:</label></div>
                                    <input type="email" id="useremail" name="useremail" placeholder="Enter Your Email address" value="<?php echo set_value('useremail'); ?>">
                                    <?php echo form_error('useremail', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="usermessage">Comment if any:</label></div>
                                    <textarea id="usermessage" name="usermessage" placeholder="Write your message"><?php echo set_value('usermessage'); ?></textarea>
                                    <?php echo form_error('usermessage', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <!-- <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="userqueries">Queries if any: </label></div>
                                    <textarea id="userqueries" name="userqueries" placeholder="Write your message"><?php echo set_value('userqueries'); ?></textarea>
                                    <?php echo form_error('userqueries', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div> -->
                                <div class="col-md-12 col-sm-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Submit</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>