<?php
class Details_register extends MX_Controller
{

function __construct() {
parent::__construct();
}


function index()
{
    //SEO
	$data['meta_title'] = "MOC | Register detailes";
	$data['meta_description'] = "Register detailes";
	$data['meta_keywords'] = "";
  //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];
            
    $data['view_module'] = "details_register";
    $data['view_file'] = "details_register";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submit()
{
	//echo "hii";
	$this->load->model('Mdl_Detailsregister');
	$this->form_validation->set_rules('name','Name','required|trim',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
  $this->form_validation->set_rules('age','Age','required|numeric',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'numeric' => '%s should not contain text.'
                        )
                    );
  $this->form_validation->set_rules('location','Location','required',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        )
                    );
  $this->form_validation->set_rules('useremail','Email Address','valid_email',
                      array(
                        'valid_email' => '%s you have entered is not valid.'
                        )
                    );
  $this->form_validation->set_rules('contact','Contact Number','required|numeric',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'numeric' => '%s should not contain text.'
                        )
                    );
  $this->form_validation->set_rules('usermessage','Message','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
  // $this->form_validation->set_rules('userqueries','Message','required',
  //                     array(
  //                       'required' => 'Please Enter %s to continue.'
  //                       )
  //                   );
  if($this->form_validation->run())
  {
  	$detailsregister = $this->input->post();
  	$this->Mdl_Detailsregister->save_detailsregister($detailsregister);
			$this->session->set_flashdata('flash_message', '<p class="alert alert-success text-center">'.'Thank you for your contacting us we will get back you soon !!'.'</p>');
      $this->send_mail($detailsregister);
			redirect('details_register');
  }
  else
  {
  	$this->index();
  }
}

public function send_mail($detailsregister)
{
    

    $this->load->library('email');

      $Useremail = 'rrcg.komal@gmail.com';
      $subject = 'Message from : '.$detailsregister['name'];
      $message = '  <center>
        <img src="http://rrcgvir.com/moc/assets/images/logo.png" alt="MOC Logo" style=" display: block; margin-left: auto; margin-right: auto;">
        <div style="width: 700px; background-color: #411e75 ; color: #fff;  padding-left: 10px; padding-top: 10px; padding-bottom:  10px; padding-right: 10px; border-radius: 5px; text-align: center; margin-top: 10px;">
            <h3>Message from : '.$detailsregister['name'].'</h3>
        </div>
            <div style="width: 700px; background-color: #f7f7f7;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
            <p style="font-size: 17px; font-weight: 600; text-align: justify; margin-top: 0px; padding-top:  15px; ">Dear '.$detailsregister['name'].',</p>
            <p style="font-size: 17px; text-align: justify;">Greetings from <a href="http://rrcgvir.com/moc/" style="text-decoration: none;"><span style="color: #ef4581;"><strong>MOC</strong></span></a>,</p>
            <p style="font-size: 17px; text-align: justify;">There is a new Contact Us Message from the user, details of message are given as below,</p>
            <table id="customers" style=" border-collapse: collapse; width: 85%;">
              <tr>
                <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Name</th>
                <td style="border: 1px solid #ddd; padding: 8px;">'.$detailsregister['name'].'</td>
              </tr>
              <tr>
                <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Email</th>
                <td style="border: 1px solid #ddd; padding: 8px;">'.$detailsregister['useremail'].'</td>
              </tr>
               <tr>
                <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                <td style="border: 1px solid #ddd; padding: 8px;">'.$detailsregister['contact'].'</td>
              </tr>
               <tr>
                <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">complications if any</th>
                <td style="border: 1px solid #ddd; padding: 8px;">'.$detailsregister['usermessage'].'</td>
              </tr>
            </table>
            <p style="font-size: 17px; text-align: justify; margin-top: 10px; padding-bottom: 15px;">Thank you.</p>
        </div>
    </center>';

      // Get full html:
      $body = '
      <!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
    body {
        font-family: "Raleway", sans-serif;
    }       

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}
</style>
</head>
<body>
          ' . $message . '
        
 </body>
</html>';


        $result = $this->email
        ->from('test@rrcgvir.com', 'MOC')
        ->reply_to('test@rrcgvir.com')    // Optional, an account where a human being reads.
        ->to($Useremail)
        ->subject($subject)
        ->message($body)
        ->send();
        //print_r($result); exit();

        echo $body; die();

 
}



}
