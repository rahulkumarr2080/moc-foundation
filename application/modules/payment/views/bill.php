<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Invoice</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li class="active">Invoice</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="container" id="printableArea">
            <!--Form Column--->
        <div class="row">
            <div class="offset-lg-2 col-lg-9 col-sm-12 bg-white shadow-lg">
                <div class="logo-image text-center">
                    <img src="<?=  base_url();?>assets/images/logo.png" alt="Logo" style="padding: 6px; margin: 10px;">    
                </div>
                <hr>
               <div class="row">
                   <div class="col-md-6">
                    <?php //print_r($billDetails); ?>
                        <p><strong>Address:</strong></p>
                        <p class="text-left"><?= $billDetails['userName'];  ?> <br><?= $billDetails['userEmail'];  ?> <br> <?= $billDetails['plan'];  ?><br>
                      <?= $billDetails['address1'];  ?> <br><?= $billDetails['address2'];  ?> <br> kalyan west <br><?= $billDetails['state'];  ?> <br><?= $billDetails['city'];  ?><br><?= $billDetails['zipcode'];  ?></p>
                   </div>
                   <div class="col-md-6">
                       <p class="text-right">ORDER #<?= $billDetails['transactionId']; ?> <br>MARCH 4TH 2016</p>
                   </div>
               </div>
               <hr>
               <div class="row">
                   <table class="table">
                       <tr>
                           <td class="text-center">
                               you  paid total Rs.<?= $billDetails['amount'];  ?>
                           </td>
                       </tr>
                   </table>
               </div>
               <button class="btn btn-info" style="padding: 5px; margin: 5px;" onclick="printDiv('printableArea')">Print Invoice</button>
            </div>    
        </div>
    </div>
</section>

  <script type="text/javascript">
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
</script>