<?php
class Payment extends MX_Controller 
{

function __construct() {
parent::__construct();
}


public function request()
    {
        
        $post = $this->input->post();   
        //print_r($post); exit();
        $data = array();
        $tid = $post['tid'];
        $this->load->model('Mdl_Payment');
        $isDuplicateMdl_PaymentID = $this->Mdl_Payment->isDuplicateTransactionID($tid);
        if($isDuplicateMdl_PaymentID == 0) //If returns 0 then there is duplicate Id
        {
            $newMdl_PaymentId = rand(11111,99999);
            $data['tid'] = $newMdl_PaymentId;
            $data['order_id'] = $newMdl_PaymentId;
        }
        else
        {   
            $newMdl_PaymentId = $tid;
            $data['tid'] = $post['tid'];
            $data['order_id'] = $post['tid'];
        }
        //print_r($data['tid']); exit();
        $data['merchant_id'] = $post['merchant_id'];
        $data['amount'] = $post['amount'];
        $data['currency'] = $post['currency'];
        $data['redirect_url'] = $post['redirect_url'];
        $data['cancel_url'] = $post['cancel_url'];
        $data['language'] = $post['language'];
        $data['id'] = $post['id'];
        //print_r($data); exit();
        $this->load->model('Mdl_Payment');
        //print_r($newMdl_PaymentId); exit();
        $this->Mdl_Payment->iniateTransaction($post,$newMdl_PaymentId);

        $data['meta_title'] = 'Payment Page';
        $data['meta_description'] = 'Payment';
        $data['meta_keywords'] = 'Payment';
        $data['view_module'] = "payment";
        $data['view_file'] = "ccavRequestHandler";
        $this->load->module('templates');
        $this->templates->public_bootstrap($data);
    }

    public function response()
    {
        $data['view_module'] = "payment";
        $data['view_file'] = "ccavResponseHandler";
        $this->load->module('templates');
        $this->templates->public_bootstrap($data);
    }

    public function aborted()
    {
        $Mdl_PaymentDetails = $this->input->post();
        $trId = $Mdl_PaymentDetails['Mdl_PaymentId'];
        echo "Mdl_Payment ID: " .$trId. " Is Cancelled";
    }

    public function success()
    {
        $Mdl_PaymentDetails = $this->input->post();
        $this->load->model('Mdl_Payment');
        $changeMdl_PaymentStatus = $this->Mdl_Payment->changeMdl_PaymentStatus($Mdl_PaymentDetails);
        if($changeMdl_PaymentStatus == 1)
        {
            $this->receipt($Mdl_PaymentDetails);
        }

    }

    public function receipt($Mdl_PaymentDetails)
    {
            $this->load->view('layout/header');
            $this->load->view('public/recepit',$Mdl_PaymentDetails);
            $this->load->view('layout/footer');     
    }
    public function failure()
    {
        $Mdl_PaymentDetails = $this->input->post();
        $trId = $Mdl_PaymentDetails['Mdl_PaymentId'];
        echo "Mdl_Payment ID: " .$trId. " Is Failed";
    }

    public function bill()
    {
        //echo "string"; die();
        $tnumber = 43541;
        $this->load->model('Mdl_Payment');
        $billDetails = $this->Mdl_Payment->generateBill($tnumber);
        //print_r($billDetails); exit();
        $data['billDetails'] = $billDetails;
        $data['meta_title'] = 'MOC | Invoice';
        $data['meta_description'] = 'Invoice';
        $data['meta_keywords'] = '';
        $data['view_module'] = "payment";
        $data['view_file'] = "bill";
        $this->load->module('templates');
        $this->templates->public_bootstrap($data);
        //$data['view_file'] = "bill";
        //$this->load->view('layout/header');
        //$this->load->view('public/billpage',$billDetails);
        //$this->load->view('layout/footer'); 
    }






}