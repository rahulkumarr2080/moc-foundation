<?php
class Contact extends MX_Controller
{

function __construct() {
parent::__construct();
}


function index()
{
    //SEO
	$data['meta_title'] = "MOC | Contact Us";
	$data['meta_description'] = "Contact us detailes";
	$data['meta_keywords'] = "";
  //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];
    $data['view_module'] = "contact";
    $data['view_file'] = "contact";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submit()
{
	//echo "hii";
	$this->load->model('Mdl_Contact');
	$this->form_validation->set_rules('name','Name','required|trim',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
  $this->form_validation->set_rules('useremail','Email Address','required|valid_email',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'valid_email' => '%s you have entered is not valid.'
                        )
                    );
  $this->form_validation->set_rules('contact','Contact Number','required|numeric',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'numeric' => '%s should not contain text.'
                        )
                    );
  $this->form_validation->set_rules('usermessage','Message','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
  if($this->form_validation->run())
  {
  	$contactDetails = $this->input->post();
  	$this->Mdl_Contact->save_contact($contactDetails);
			$this->session->set_flashdata('flash_message', '<p class="alert alert-success text-center">'.'Thank you for your contacting us we will get back you soon !!'.'</p>');
			redirect('contact');
  }
  else
  {
  	$this->index();
  }
}




}
