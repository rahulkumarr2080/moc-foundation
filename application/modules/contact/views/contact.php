<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Contact Us</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a>
                </li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column--->
            <div class="form-column col-lg-8 col-md-8 col-sm-12">
                <div class="inner">
                    <div class="default-form contact-form">
                        <form method="post" action="<?= base_url(); ?>contact/submit" id="contact-form">
                            <?php if ($this->session->flashdata('flash_message')): ?>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                            <?php endif; ?>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="name">Name:</label></div>
                                    <input type="text" id="name" name="name" placeholder="Enter Your Name" value="<?php echo set_value('name'); ?>">
                                    <?php echo form_error('name', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="useremail">Email:</label></div>
                                    <input type="email" id="useremail" name="useremail" placeholder="Enter Your Email address" value="<?php echo set_value('useremail'); ?>">
                                    <?php echo form_error('useremail', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="contact">Phone:</label></div>
                                    <input type="text" id="contact" name="contact" placeholder="Enter Your Contact Number" value="<?php echo set_value('contact'); ?>">
                                    <?php echo form_error('contact', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="usermessage">Message:</label></div>
                                    <textarea id="usermessage" name="usermessage" placeholder="Write your message"><?php echo set_value('usermessage'); ?></textarea>
                                    <?php echo form_error('usermessage', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Send Message</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="form-column col-lg-4 col-md-4 col-sm-12">
                <h2 class="main-headh2">Address</h2>
                <p><span class="fa fa-map">&nbsp;&nbsp;&nbsp;</span>1st floor, Khodiyar Apartment, <br>Daulat Nagar, Road number 6, Close to Sudhir Phadke Flyover, <br>Borivali East, Mumbai – 400066.</p>
                <p><span class="fa fa-phone-alt">&nbsp;&nbsp;&nbsp;</span>Phone +91 86574 90578</p>
                <p><span class="fa fa-envelope-open">&nbsp;&nbsp;&nbsp;</span>msw@mocindia.co.in</p>
            </div>
        </div>
    </div>
</section>