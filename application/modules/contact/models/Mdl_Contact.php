<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_Contact extends CI_Model
{

function __construct() {
parent::__construct();
}

public function save_contact($contactDetails)
{
	$this->db->insert('contact',['name'=>$contactDetails['name'],'email'=>$contactDetails['useremail'],'contact'=>$contactDetails['contact'],'message'=>$contactDetails['usermessage'],'date'=>time()]);
}

}
