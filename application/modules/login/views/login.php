<!--Page Title-->
<section>
  <div class="">
    <img src="<?= base_url();?>assets/img/Member.jpg" alt="" />
  </div>
</section>
<!--End Page Title-->
<div class="container pt-5 pb-5 center">
 <div class="row pt-5 pb-5">
    <div class="col-md-5 col-sm-12 pt-5 pb-5 border formdesign">
      <?php if ($this->session->flashdata('login_failed')): ?>
      <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
      <?php endif; ?>

      <?php echo form_open('login/submit') ?>
      <div class="form-group">
      <label for="email">User Name:</label>
      <?php echo form_input(['name'=>'username','class'=>'form-control inputform','placeholder'=>'Username','value'=>set_value('username')]) ?>
      <?php echo form_error('username', '<div class="text-danger">', '</div>'); ?>
     <!--<input type="email" class="form-control" id="email" placeholder="Email" name="email">-->
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <?php echo form_password(['name'=>'password','class'=>'form-control inputform','placeholder'=>'Password']) ?>
      <?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
      <!--<input type="password" class="form-control" id="pwd" placeholder="Password" name="pwd">-->
    </div>
    <!--<div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>-->
    
    <?php echo 
         
         form_submit(['name'=>'submit','value'=>'Login','class'=>'btn btn-success inputbtn']);
    ?><br><br>
   <!-- <a href="<?= base_url();?>login/forgot-password" class="memberlink">Forgot your password?</a>-->
    <!--<button type="submit" class="btn btn-default">Submit</button>-->
  </form>

</div>
<div class="col-md-7">
    <img src="<?= base_url();?>assets/img/form-bg.jpg" alt="" class="p-4" />
  
</div>
</div>
</div>