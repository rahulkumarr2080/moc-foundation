<?php
class Login extends MX_Controller 
{

function __construct() {
parent::__construct();
}

public function index()
{
    //SEO
    $data['meta_title'] = 'MOC | Login';
    $data['meta_description'] = 'Login.';
    $data['meta_keywords'] = '';
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "login";
    $data['view_file'] = "login";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submit()
{      
       //SEO
       $data['meta_title'] = 'MOC | Login';
       $data['meta_description'] = 'Login.';
       $data['meta_keywords'] = '';
       //Facebook Open Graph
       $data['meta_ogimage'] = "";
       $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
              "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
              $_SERVER['REQUEST_URI'];

       $this->form_validation->set_rules('username','User Name','required|alpha|trim');
       $this->form_validation->set_rules('password','password','required');
       
       if( $this->form_validation->run() )
       { //if validation passes 
           //success.
           $username = $this->input->post('username');
           $password = $this->input->post('password');
           //echo "username: $username and password: $password";
           //echo "validation Successful.";
           $this->load->model('Mdl_Login');

           $login_id = $this->Mdl_Login->login_valid($username, $password);
           if( $login_id )
           {
             
              $this->session->set_userdata('user_id', $login_id );
              return redirect('admin/articles');//////////////////////////////////////////////////////////
              //$this->load->view('admin/dashboard');
            //credentials valid, login user.
           }
           else
           {
             $this->session->set_flashdata('login_failed', "Invalid Username/Password.");
             return redirect('login');
             // echo "Password Do Not Match";
            //authentication faild.
           }
       }
        else {
            $data['view_module'] = "login";
            $data['view_file'] = "login";
            $this->load->module('templates');
            $this->templates->public_bootstrap($data);
       }
}

}