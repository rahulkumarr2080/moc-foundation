<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_Transaction extends CI_Model
{

function __construct() {
parent::__construct();
}
public function isDuplicateTransactionID($tid)
        {
            $query = $this->db->query("SELECT * FROM `transaction` WHERE tid = '$tid' ");
            $row = $query->row();
            if($row)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public function iniateTransaction($post,$newMdl_TransactionId)
        {
            $userId = $post['id'];
            $transactionId = $newMdl_TransactionId;
            $amount = $post['amount'];
            $date = time();
            $run = $this->db->insert('transaction',['tid'=>$transactionId,'uid'=>$userId,'date'=>$date,'amount'=>$amount]);
        }
         public function changeTransactionStatus($transactionDetails,$trackingId)
         {
            $transactionID = $transactionDetails['transactionId'];
            $trackingId = $transactionDetails['trackingId'];
            $trStatus = $transactionDetails['status'];
            $changeTransactionStatus = $this->db->set('status', 'Done') //value that used to update column  
                                                ->set('tracking', $trackingId) //value that used to update column  
                                    ->where('tid', $transactionID) //which row want to upgrade  
                                        ->update('transaction');  //table name
            if($changeTransactionStatus == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
         }

         public function generateBill($tnumber)
         {
            //echo "string ".$tnumber;
            $data = array();

            //retrieve Data From Transaction Table
            $query = $this->db->select('*')
                                ->where('tid', $tnumber)
                                ->get('transaction');
            $row = $query->row_array();         
            $userId = $row['uid'];


            //Retrive Data From Userb Table
            $query2 = $this->db->select('*')
                                ->where('id', $userId)
                                ->get('usersb');
            $row2 = $query2->row_array();   
            
            $data['userId'] = $userId;
            $data['userName'] = $row2['name'];
            $data['userEmail'] = $row2['email'];
            $data['plan'] = $row2['contact'];
            $data['transactionId'] = $row['tid'];
            $data['amount'] = $row['amount'];
            $data['status'] = $row['status'];

            //print_r($data);
            return $data;

            
         }
         public function get_user_id($transactionId)
         {
                $sql = "SELECT * FROM `transaction` WHERE tid =  '$transactionId'";
                $result = $this->db->query($sql);
                $row = $result->row();
                return $row->uid;
         }
         
         public function get_user_cartDetails($getUserID)
         {
             $query = $this->db->query("SELECT * FROM `store_basket` WHERE user_id = '$getUserID' AND status = 'pending'");
            return $query->result();
         }
        
        public function change_user_cartDetails($getUserID)
        {
           $this->db->set('status', 'complete') //value that used to update column
                                    ->where('user_id', $getUserID) //which row want to upgrade  
                                        ->update('store_basket');  //table name 
        }

        public function get_amount($transactionId)
        {
            $sql = "SELECT * FROM `transaction` WHERE tid =  '$transactionId'";
                $result = $this->db->query($sql);
                $row = $result->row();
                return $row->amount;
        }

           public function get_date($transactionId)
            {
                    $sql = "SELECT * FROM `transaction` WHERE tid =  '$transactionId'";
                    $result = $this->db->query($sql);
                    $row = $result->row();
                    return $row->date;
            }

}
