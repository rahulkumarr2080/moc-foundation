<?php
class Transaction extends MX_Controller 
{

function __construct() {
parent::__construct();
}


public function request()
    {
        
      $post = $this->input->post();   
        //print_r($post); exit();
        $data = array();
        $tid = $post['tid'];
        $this->load->model('Mdl_Transaction');
        $isDuplicateTransactionID = $this->Mdl_Transaction->isDuplicateTransactionID($tid);
        if($isDuplicateTransactionID == 0) //If returns 0 then there is duplicate Id
        {
            $newTransactionId = rand(11111,99999);
            $data['tid'] = $newTransactionId;
            $data['order_id'] = $newTransactionId;
        }
        else
        {   
            $newTransactionId = $tid;
            $data['tid'] = $post['tid'];
            $data['order_id'] = $post['order_id'];
        }
        //print_r($data['tid']); exit();
        $data['merchant_id'] = $post['merchant_id'];
        $data['amount'] = $post['amount'];
        $data['currency'] = $post['currency'];
        $data['redirect_url'] = $post['redirect_url'];
        $data['cancel_url'] = $post['cancel_url'];
        $data['language'] = $post['language'];
        $data['id'] = $post['id'];
        //print_r($data); exit();
        $this->load->model('Mdl_Transaction');
        $this->Mdl_Transaction->iniateTransaction($post,$newTransactionId);

        
        $data['view_module'] = "transaction";
        $data['view_file'] = "ccavRequestHandler";
        $this->load->module('transaction');
        $this->load->module('templates');
        $this->templates->public_bootstrap($data);
    }

    public function response()
    {
        $data['view_module'] = "transaction";
        $data['view_file'] = "ccavResponseHandler";
        $this->load->module('templates');
        $this->templates->public_bootstrap($data);
    }

    public function aborted()
    {
         $transactionDetails = $this->input->post();
         //print_r($transactionDetails); 
        $trId = $transactionDetails['transactionId'];
        echo "Transaction ID: " .$trId. " Is Cancelled <br>";
        echo "Tracking Id: " .$transactionDetails['trackingId']. "<br>";
        echo "Status: " .$transactionDetails['status']. "<br>";
    }

    public function success()
    {
        $transactionDetails = $this->input->post();
        print_r($transactionDetails); exit();
        $this->load->model('Mdl_Transaction');
        $changeTransactionStatus = $this->Mdl_Transaction->changetransactionStatus($transactionDetails);
        if($changeTransactionStatus == 1)
        {
            $this->receipt($transactionDetails);
        }

    }

    public function receipt()
    {
        $data['view_module'] = "transaction";
        $data['view_file'] = "receipt";
        //$data['transactionDetails'] = $transactionDetails;
        $transactionId = 30371;
        $this->load->module('templates');
        $this->load->module('transaction');
        $this->load->model('Mdl_Transaction');
        $getUserID = $this->Mdl_Transaction->get_user_id($transactionId);
        //print_r($getUserID); exit();
        $getUserOrderDetails = $this->Mdl_Transaction->get_user_cartDetails($getUserID);
        $data['getUserOrderDetails'] = $getUserOrderDetails;
        $data['getUserID'] = $getUserID;
        $data['transactionId'] = $transactionId;
        $this->templates->public_bootstrap($data);
        //$this->Mdl_Transaction->change_user_cartDetails($getUserID);
        
            //$this->load->view('layout/header');
            //$this->load->view('public/recepit',$Mdl_TransactionDetails);
            //$this->load->view('layout/footer');     
    }
    public function failure()
    {
       $transactionDetails = $this->input->post();
         //print_r($transactionDetails); 
        $trId = $transactionDetails['transactionId'];
        echo "Transaction ID: " .$trId. " Is Failed <br>";
        echo "Tracking Id: " .$transactionDetails['trackingId']. "<br>";
        echo "Status: " .$transactionDetails['status']. "<br>";
    }
    public function othererror()
    {
        $transactionDetails = $this->input->post();
         //print_r($transactionDetails); 
        $trId = $transactionDetails['transactionId'];
        echo "Transaction ID: " .$trId. " Is Not Secure  <br>";
        echo "Tracking Id: " .$transactionDetails['trackingId']. "<br>";
        echo "Status: " .$transactionDetails['status']. "<br>";
    }

    public function bill()
    {
        $tnumber = 26611;
        $this->load->model('Mdl_Transaction');
        $billDetails = $this->Mdl_Transaction->generateBill($tnumber);

        $this->load->view('layout/header');
        $this->load->view('public/billpage',$billDetails);
        $this->load->view('layout/footer'); 
    }






}