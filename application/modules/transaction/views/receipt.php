<div class="container" id="printableArea">
     
      <div class="row pad-top-botm ">
         <div class="col-lg-6 col-md-6 col-sm-6 ">
            <img src="<?php echo base_url();?>assets/images/logo.png" style="padding-bottom:20px;"> 
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            
               <strong>Brian Bossier Design</strong>
              <br>
                  <i>Address :</i> Barrington, IL
              <br>
                  89th street , Suite 69,
              <br>
                  United States.
              
         </div>
     </div>
     <div class="row text-center contact-info">
         <div class="col-lg-12 col-md-12 col-sm-12">
             <hr>
             <span>
                 <strong>Email : </strong>  info@manuel.co.in 
             </span>
             <span>
                 <strong>Call : </strong>  +9999999999
             </span>
              <span>
                 <strong>Fax : </strong>  +012340-908- 890 
             </span>
             <hr>
         </div>
     </div>
     <div class="row pad-top-botm client-info">
         <div class="col-lg-6 col-md-6 col-sm-6">
         <h4>  <strong>Client Information</strong></h4>
           <strong> Classy Client</strong>
             <br>
                  <b>Address :</b> 111 , their street name,
              <br>
                 United States.
             <br>
             <b>Call :</b> +1-908-567-0987
              <br>
             <b>E-mail :</b> info@clientdomain.com
         </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            
               <h4>  <strong>Payment Details </strong></h4>
               <?php 
                $this->load->module('transaction');
                $this->load->model('Mdl_Transaction');
                ?>
            <b>Bill Amount :  <?php echo $this->Mdl_Transaction->get_amount($transactionId); ?> </b>
              <br>
               Bill Date :  <span> <?php $the_date = date('l jS \of F Y', $this->Mdl_Transaction->get_date($transactionId));
                                                     echo $the_date; ?></span>
              <br>
               <b>Payment Status :  <?php echo "paid";//$transactionDetails['status']; ?> </b>
               <br>
         </div>
     </div>
     <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
           <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sr. Number:</th>
                                    <th>Name:</th>
                                    <th>Quantity:</th>
                                    <th>Unit Price:</th>
                                     <th>Sub Total:</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($getUserOrderDetails as $row): ?>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $row->product_title; ?></td>
                                    <td><?php echo $row->product_qty; ?></td>
                                    <td>$435 USD</td>
                                    <td>Rs. <?php echo $row->price; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
               </div>
             <hr>
             <div class="ttl-amts">
               <h5>  Total Amount : Rs. <?php echo $this->Mdl_Transaction->get_amount($transactionId); ?> </h5>
             </div>
             <hr>
              <div class="ttl-amts">
                  <h5>  Tax : 90 USD ( by 10 % on bill ) </h5>
             </div>
             <hr>
              <div class="ttl-amts">
                  <h4> <strong>Bill Amount : Rs. <?php echo $this->Mdl_Transaction->get_amount($transactionId); ?></strong> </h4>
             </div>
         </div>
     </div>
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <strong> Important: </strong>
             <ol>
                  <li>
                    This is an electronic generated invoice so doesn't require any signature.

                 </li>
                 <li>
                     Please read all terms and polices on  www.yourdomaon.com for returns, replacement and other issues.

                 </li>
             </ol>
             </div>
         </div>
      <div class="row pad-top-botm">
         <div class="col-lg-12 col-md-12 col-sm-12">
             <hr>
             </div>
         </div>
 </div>
 <div class="container">
   <button class="btn btn-primary btn-lg" onclick="printDiv('printableArea')">Print Invoice</button>
 </div>


  <script type="text/javascript">
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
</script>