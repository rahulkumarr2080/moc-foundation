<section id="page-title">
   <div class="container clearfix">
      <h1>FAQs</h1>
      <span>All your Questions answered in one place</span>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="#">Home</a></li>
         <li class="breadcrumb-item active" aria-current="page">FAQs</li>
      </ol>
   </div>
</section>
<section id="content">
   <div class="content-wrap">
      <div class="container clearfix">
         <h3>Some of your Questions:</h3>
         <p>Please read all the Faq's of if you have any questions regarding using the product</p>
         <div class="divider"><i class="icon-circle"></i></div>
         <div class="nobottommargin">
            <div class="accordion accordion-border clearfix" data-state="closed">
               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>Does Tyteen have harmful impact on kidneys?</div>
               <div class="acc_content clearfix">Consistent high dose consumption of proteins are known to have harmful impact on Kidneys. But this should not keep one away from having protein supplement, especially those whose daily requirement of nutrition is not fulfilled through their diet. Tyteen does not produce any damage to kidneys as it is only 40% protein in 100 gm. Tyteen is recommended to be consumed when needed by the body the most. Consistent consumption is not required hence not recommended. Tyteen ensures quickest recovery and in no time. It is the only protein & nutrition supplement in the market that do not support continuous consumption. Tyteen is recommended to be consumed in an intermittent fashion (Have-Stop-Have) in order to optimize its effect. One can adapt this protocol as ‘1 month consumption followed by 2 months gap’. Tyteen is not recommended in patients who are suffering with or under the treatment for Kidney disease.</div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>Does Tyteen have steroids?</div>
               <div class="acc_content clearfix">We do not use Steroids in any form or quantity in Tyteen.</div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>Is Tyteen suitable for Diabetic and Hypertensive (Blood Pressure) patients?</div>
               <div class="acc_content clearfix">Tyteen is Sugar and Gluten free hence highly recommended in Diabetic patients.</div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>Does regular use of proteins increase chance of developing cancer? </div>
               <div class="acc_content clearfix">There is a warning and a recommendation from American Association of Oncology that long term use of Soya protein increases of developing cancer. Hence Tyteen uses 100% whey protein which is safe among all sources of proteins for consumption, even long term for that matter. Whey protein is a simplest form of protein which has a peculiar ability to get absorbed quickly, ensure quick results</div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>Can children and / or adolescents consume Tyteen?</div>
               <div class="acc_content clearfix">We do not recommend Tyteen in children aged below 10 years. All above 10 can consume it.</div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>What is recommended amount of Tyteen for daily consumption?</div>
               <div class="acc_content clearfix"></div>

               <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>How long can anyone store Tyteen?</div>
               <div class="acc_content clearfix">Tyteen has a group of active digestive enzymes (Stomazyme) in it, used for improving overall digestion in consumers. These enzymes are active in different acidic levels inside stomach (wide range of pH). Hence unlike other nutrition supplements in the market, Tyteen has a relatively short expiry of 2 years only. It is recommended not to store Tyteen beyond prescribed expiry.</div>
               
               
            </div>
         </div>
      </div>
   </div>
</section>