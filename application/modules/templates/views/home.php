<!-- Banner Section -->
<section class="banner-section">
    <div class="banner-carousel kausid-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 0, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 300, "responsive":{ "0" :{ "items": "1" }, "768" :{ "items" : "1" } , "1000":{ "items" : "1" }}}'>
        <!-- Slide Item -->
        <div class="slide-item">
             <div class="container-fluid">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title"></div>
                            <h2>Working towards Preventing People from Falling Prey to Cancer</h2>
                            <div class="links-box"><a href="#" class="theme-btn btn-style-two"><span class="btn-title">Join Hands</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="image-layer" style="background-image:url('assets/images/slider/1.jpg')"></div>
           
        </div>
        <!-- Slide Item -->
        <div class="slide-item">
            <div class="image-layer" style="background-image:url('assets/images/slider/2.jpg')"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title"></div>
                            <h2>Working towards Improving Access to Cancer Treatment</h2>
                            <div class="links-box"><a href="#" class="theme-btn btn-style-two"><span class="btn-title">Join Hands</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slide Item -->
        <div class="slide-item">
            <div class="image-layer" style="background-image:url('assets/images/slider/3.jpg')"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title"></div>
                            <h2>Working towards Treatment and Rehabilitation of Cancer Patients</h2>
                            <div class="links-box"><a href="#" class="theme-btn btn-style-two"><span class="btn-title">Join Hands</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<!--End Banner Section -->
<!--About Section-->
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Left Column-->
            <div class="left-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner">
                    <div class="sec-title">
                        <h2>Why MOC Cancer Care Foundation?</h2>
                    </div>
                    <div class="lower-box">
                        <div class="text">In India, it is estimated and reported by government and various independent cancer registries of India that cancer incidence are rapidly growing due increasing urbanization, rapid adoption of western lifestyle as well as rampant and atrocious addictions like tobacco in Indian population.</div>
                        <div class="link-box"><a href="<?= base_url();?>about/about-us" class="default-link">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--Right Column-->
            <div class="right-column col-lg-5 col-md-12 col-sm-12">
                <div class="about-feature-box">
                    <div class="outer clearfix">
                        <!--About Feature-->
                        <div class="about-feature col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="content-box">
                                    <div class="inner">
                                        <h3><a href="#">Patient Consent Form</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--About Feature-->
                        <div class="about-feature col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="content-box">
                                    <div class="inner">
                                        <h3><a href="<?= base_url();?>our-work/guidelines">Charity Guidelines</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--About Feature-->
                        <div class="about-feature col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="content-box">
                                    <div class="inner">
                                        <h3><a href="#">Offline Donation Form</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--About Feature-->
                        <div class="about-feature col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="content-box">
                                    <div class="inner">
                                        <h3><a href="<?= base_url();?>assets/donor-donation-form-moc-foundation.pdf" download>MOC Form Download</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="padding-top: 30px;padding-bottom: 30px;">
    <div class="sponsors-outer">
        <div class="auto-container">
            <h2 class="main-headh2">Our Trustees</h2>
            <!--Sponsors Carousel-->
            <div class="kausid-carousel owl-theme owl-carousel" data-options='{"loop": false, "margin": 40, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 300, "responsive":{ "0" :{ "items": "1" }, "600" :{ "items" : "2" }, "768" :{ "items" : "3" } , "800":{ "items" : "3" }, "1024":{ "items" : "4" }}}'>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>about/trustees"><img src="<?= base_url();?>assets/images/home/vashistha-pankaj.jpg" alt="vashistha-pankaj"></a>
                    </figure>
                    <h4 class="icon-headd">Dr. Vashishth Maniar</h4>
                </div>
                
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>about/trustees"><img src="<?= base_url();?>assets/images/home/ashish-joshi.jpg" alt="ashish-joshi"></a>
                    </figure>
                    <h4 class="icon-headd">Dr. Ashish Joshi</h4>
                </div>
                
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>about/trustees"><img src="<?= base_url();?>assets/images/home/pritam-kalaskar.jpg" alt="pritam-kalaskar"></a>
                    </figure>
                    <h4 class="icon-headd">Dr. Pritam Kalaskar</h4>
                </div>
            
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>about/trustees"><img src="<?= base_url();?>assets/images/home/kshitij-joshi.jpg" alt="kshitij-joshi"></a>
                    </figure>
                    <h4 class="icon-headd">Dr. Kshitij Joshi</h4>
                </div>
                
            </div>
        </div>
    </div>
</section>
<section style="padding-top: 50px; padding-bottom: 50px; background-color: #eef7f8;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="main-headh2">Donate For A Cause</h2>
                <p class="text-center">We all understand that the collaborative efforts will be required from various well-to-do sections of the society to help economically / financially backward cancer patients and their relatives live a healthy and a respectful life. We invite wishful donors to come forward and donate to MOC Cancer Care Foundation so that we can help more & more cancer patients complete their cancer treatments and contribute to the wellbeing of them and their families. We urge you to partner with MOC Cancer Care Foundation & strengthen this public health initiative.</p>
                <div class="text-center"><a href="#" class="theme-btn btn-style-one"><span class="btn-title">Donate Now</span></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- icon section dekstop view -->
<section class="icon-section d-none d-lg-block" style="background-image:url('assets/images/slider/6.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-5">.</div>
            <div class="col-md-7">
                <div class="icon-group-home pt-5 pb-5">
                    <div class="row pt-3">
                        <div class="col-md-3 pb-5">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/awarness.png" alt="awarness" title="awarness" class="img-fluid wow zoomIn" data-wow-delay=".1s"></a>
                            <h4 class="icon-head-mob">Awarness</h4>
                        </div>
                        <div class="col-md-3 pb-5">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/education.png" alt="education" title="education" class="img-fluid wow zoomIn" data-wow-delay=".2s"></a>
                            <h4 class="icon-head-mob">Education</h4>
                        </div>
                        <div class="col-md-3 pb-5">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/medication.png" alt="medication" title="medication" class="img-fluid wow zoomIn" data-wow-delay=".3s"></a>
                            <h4 class="icon-head-mob">Medicines</h4>
                        </div>
                        <div class="col-md-3 pb-5">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/treatment.png" alt="treatment" title="treatment" class="img-fluid wow zoomIn" data-wow-delay=".4s"></a>
                            <h4 class="icon-head-mob">Treatment</h4>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-3 pb-3">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/children.png" alt="children" title="children" class="img-fluid wow zoomIn" data-wow-delay=".5s"></a>
                            <h4 class="icon-head-mob">Children</h4>
                        </div>
                        <div class="col-md-3 pb-3">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/donate.png" alt="donate" title="donate" class="img-fluid wow zoomIn" data-wow-delay=".6s"></a>
                            <h4 class="icon-head-mob">Donate</h4>
                        </div>
                        <div class="col-md-3 pb-3">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/login.png" alt="login" title="login" class="img-fluid wow zoomIn" data-wow-delay=".7s"></a>
                            <h4 class="icon-head-mob">Login or Register</h4>
                        </div>
                        <div class="col-md-3 pb-3" style="padding-top: 10px;">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/download-form.png" alt="download-form" title="download-form" class="img-fluid wow zoomIn" data-wow-delay=".8s"></a>
                            <h4 class="icon-head-mob">Donate Offline</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- icon section mobile view -->
<section class="icon-section d-block d-lg-none" style="background-image:url('assets/images/slider/banner-icon-mobile.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="icon-group-home pt-5 pb-5">
                    <div class="row pt-3">
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/awarness.png" alt="awarness" title="awarness" class="img-fluid wow zoomIn" data-wow-delay=".1s"></a>
                            <h4 class="icon-head-mob">Awarness</h4>
                        </div>
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/education.png" alt="education" title="education" class="img-fluid wow zoomIn" data-wow-delay=".2s"></a>
                            <h4 class="icon-head-mob">Education</h4>
                        </div>
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/medication.png" alt="medication" title="medication" class="img-fluid wow zoomIn" data-wow-delay=".3s"></a>
                            <h4 class="icon-head-mob">Medicines</h4>
                        </div>
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/treatment.png" alt="treatment" title="treatment" class="img-fluid wow zoomIn" data-wow-delay=".4s"></a>
                            <h4 class="icon-head-mob">Treatment</h4>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/children.png" alt="children" title="children" class="img-fluid wow zoomIn" data-wow-delay=".5s"></a>
                            <h4 class="icon-head-mob">Children</h4>
                        </div>
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/donate.png" alt="donate" title="donate" class="img-fluid wow zoomIn" data-wow-delay=".6s"></a>
                            <h4 class="icon-head-mob">Donate</h4>
                        </div>
                        <div class="col-md-3 pb-4">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/login.png" alt="login" title="login" class="img-fluid wow zoomIn" data-wow-delay=".7s"></a>
                            <h4 class="icon-head-mob">Login or Register</h4>
                        </div>
                        <div class="col-md-3 pb-4" style="padding-top: 10px;">
                            <a href="#" title=""><img src="<?= base_url();?>assets/images/icon/download-form.png" alt="download-form" title="download-form" class="img-fluid wow zoomIn" data-wow-delay=".8s"></a>
                            <h4 class="icon-head-mob">Donate Offline</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="overlay">
  <div class="videoBox" id="videobox">
    <a class="close">X</a>
    <!-- <a href="<?= base_url();?>details-register"><img src="<?= base_url();?>assets/images/home/popup.jpg" alt="mr-joy-moc"></a> -->
    <iframe width="100%" height="480" src="https://www.youtube.com/embed/SIk-FwK0Tjs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
</div>