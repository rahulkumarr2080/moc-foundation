<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title><?= $meta_title ?></title>
<meta name="description"  content="<?= $meta_description; ?>">
<meta name="keywords"  content="<?= $meta_keywords; ?>">
<!-- Facebook meta code -->
    <meta property="og:url"            content="<?= $link ?>" />
    <meta property="og:type"           content="website" />
    <meta property="og:title"          content="<?= $meta_title ?>" />
    <meta property="og:description"    content="<?= $meta_description; ?>" />
    <meta property="og:image"          content="<?= $meta_ogimage; ?>" />
    <!-- Twitter meta code -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@moc" />
    <meta name="twitter:creator" content="@username" />
    <meta property="og:url" content="<?= $link ?>" />
    <meta property="og:title" content="<?= $meta_title ?>" />
    <meta property="og:description" content="<?= $meta_description; ?>" />
    <meta property="og:image" content="<?= $meta_ogimage; ?>" /> 
<!-- Stylesheets -->
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">
    <!-- Responsive File -->
    <link href="<?= base_url();?>assets/css/responsive.css" rel="stylesheet">

<!----link for gallary----->
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/gallery-grid.css">

<!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="#">
    <link rel="icon" type="image/png" href="<?= base_url();?>assets/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?= base_url();?>assets/favicon/favicon-16x16.png" sizes="16x16">

<!-- Responsive Settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <!--<div class="preloader"><div class="icon"></div></div>-->

    <!-- Main Header -->
    <header class="main-header header-style-one">
        <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner clearfix">
                    <div class="top-left clearfix">
                        <ul class="info clearfix">
                            <li><a href="tel:812-070-3692"><span class="icon fa fa-phone-alt"></span> +91 86574 90578</a></li>
                            <li><a href="mailto:mocf@mocfoundation.com"><span class="icon fa fa-envelope-open"></span> Email: &nbsp;mocf@mocfoundation.com</a></li>
                        </ul>
                    </div>
    
                    <div class="top-right clearfix">
                        <!-- <ul class="info clearfix">
                            <?php 
                                if($this->session->userdata('user_id') == null)
                                {
                                    ?>
                                    <li><a href="<?= base_url();?>user/login"><span class="icon fa fa-sign-in-alt"></span> Login or Register</a></li>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <li><a href="<?= base_url();?>user/logout"><span class="icon fa fa-sign-in-alt"></span>Logout</a></li>
                                    <?php   
                                }
                             ?>
                            
                            <li>
                               
                            </li>
                        </ul> -->
                        <ul class="social-links clearfix">
                            <li><a href="https://www.facebook.com/mumbaioncocareindia/" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
                            <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header Upper -->
        <div class="header-upper">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <!--Logo-->
                    <div class="logo-box">
                        <div class="logo"><a href="<?= base_url();?>" title="MOC"><img src="<?= base_url();?>assets/images/logo.png" alt="MOC Cancer Care Foundation" title="MOC Cancer Care Foundation"></a></div>
                    </div>

                    <!--Nav Box-->
                    <div class="nav-outer clearfix">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler"><span class="icon flaticon-menu-1"></span></div>

                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="<?php if($this->uri->segment(1) == ""){echo "current dropdown";}?>"><a href="<?= base_url();?>">Home</a>
                                    </li>
                                    <li class="dropdown <?php if($this->uri->segment(1) == "about"){echo "current dropdown";}?>"><a href="#">About Us</a>
                                        <ul>
                                            <li><a href="<?= base_url();?>about/about-us">About Us</a></li>
                                            <li><a href="<?= base_url();?>about/trustees">Our Trustees</a></li>
                                            <li><a href="<?= base_url();?>about/donors">Our Donors</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown <?php if($this->uri->segment(1) == "our-work"){echo "current dropdown";}?>"><a href="#">Our Works</a>
                                        <ul>
                                            <li><a href="#">Works</a></li>
                                            <li><a href="<?= base_url();?>our-work/guidelines">Guidelines</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown <?php if($this->uri->segment(1) == "user"){echo "current dropdown";}?>"><a href="#">Donate</a>
                                        <ul>
                                            <!-- <li><a href="<?= base_url();?>user/login">Login / Register</a></li> -->
                                            <li><a href="#">Donate Online</a></li>
                                            <li><a href="#">Donate Offline</a></li>
                                        </ul>
                                    </li>
                                     <li class="<?php if($this->uri->segment(1) == "gallery"){echo "current dropdown";}?>"><a href="<?= base_url();?>gallery/event">Gallery</a></li> 
                                    <!-- <li><a href="#">Gallery</a></li> -->
                                    <li class="<?php if($this->uri->segment(1) == "contact"){echo "current";}?>"><a href="<?= base_url();?>contact">Contact Us</a></li>
                                </ul>

                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                        <div class="donate-link">
                            <a href="#" class="theme-btn btn-style-one"><span class="btn-title">Donate Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="<?= base_url();?>" title=""><img src="<?= base_url();?>assets/images/sticky-logo.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://www.facebook.com/mumbaioncocareindia/" target="_blank"><span class="fab fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->
        <!--side space start-->
        <div class="navik-side-content overflowXHidden">
        <!-- End Header -->
        <!-- Entire content wrapper -->
        <?php if(isset($view_file)) { $this->load->view($view_module.'/'.$view_file); } ?></div>
    </div><!--side space end-->

    
    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="image-layer " style=""></div>

        <div class="auto-container ">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                 <div class="column col-lg-4 col-md-12 col-sm-12">
                                <div class="footer-widget info-widget">
                                    <div class="widget-content">
                                        <h3>Contact Us</h3>
                                        <ul class="contact-info">
                                            <li>1st floor, Khodiyar Apartment, Daulat Nagar, <br>Road number 6, Close to
                                            Sudhir Phadke Flyover,<br> Borivali East, Mumbai – 400066.</li>
                                            <li><a href="#"><span class="fa fa-phone-alt"></span> Phone  +91 86574 90578</a></li>
                                            <li><a href="mailto:mocf@mocfoundation.com"><span class="fa fa-envelope-open"></span> mocf@mocfoundation.com</a></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>
                    <!--Column-->
                    <div class="big-column col-lg-8 col-md-12 col-sm-12">
                        <div class="row clearfix">

                            <!--Column-->
                            <div class="column col-lg-4 col-md-4 col-sm-12">
                                <div class="footer-widget links-widget">
                                    <div class="widget-content">
                                        <h3>About Us</h3>
                                        <ul>
                                            <li><a href="<?= base_url();?>about/about-us">Abour Moc</a></li>
                                            <li><a href="<?= base_url();?>about/donors">Our Donors</a></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>
                            
                            <!--Column-->
                            <div class="column col-lg-4 col-md-4 col-sm-12">
                                <div class="footer-widget links-widget">
                                    <div class="widget-content">
                                        <h3>Quick Links</h3>
                                        <ul>
                                            <li><a href="#">Donate</a></li>
                                            <li><a href="#">Query</a></li>
                                            <li><a href="<?= base_url();?>contact">Contact Us</a></li>
                                            <li><a href="#">Terms &amp; Conditions.</a></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>

                            <!--Column-->
                    <div class="column big-column col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-content">
                                <h3>Follow Us</h3>
                                <ul class="social-links clearfix">
                                    <li><a href="https://www.facebook.com/mumbaioncocareindia/" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--Column-->

                        </div>
                    </div>
                    
                   
                    
                </div>
                
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="inner">
                    <!--Copyright-->
                    <div class="copyright">Copyright © MOC 2018. Created By <a href="https://www.riverroute.in/" target="_blank">River Route Creative Group</a></div>
                </div>
            </div>
        </div>
        
    </footer>


</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="<?= base_url();?>assets/js/jquery.js"></script>
<script src="<?= base_url();?>assets/js/popper.min.js"></script>
<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/js/jquery-ui.js"></script>
<script src="<?= base_url();?>assets/js/jquery.fancybox.js"></script>
<script src="<?= base_url();?>assets/js/owl.js"></script>
<script src="<?= base_url();?>assets/js/appear.js"></script>
<script src="<?= base_url();?>assets/js/wow.js"></script>
<script src="<?= base_url();?>assets/js/scrollbar.js"></script>
<script src="<?= base_url();?>assets/js/validate.js"></script>
<script src="<?= base_url();?>assets/js/paroller.js"></script>
<script src="<?= base_url();?>assets/js/custom-script.js"></script>
<!-- wow script -->
<script>
  new WOW().init(); //for animation
</script>
<!-- wow script end -->
<!--gallary-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
<style type="text/css">
.overlay {
  width: 100%;
  background: rgba(0,0,0,.75);
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  z-index: 999;
}
.videoBox {
  position: fixed;
  width: 90%;
  left: 50%;
  top:58%;
  transform: translateY(-50%) translateX(-50%);
  padding: 20px;
  background: #fff;
  text-align: center;
  border-radius: 5px;
}
.videoBox video {
  width: 100%;
}
.close {
     width: 36px;
    height: 27px;
    position: absolute;
    top: -7px;
    right: -18px;
    display: block;
    background: url(http://localhost/moc1/assets/images/home/close-btn.png) no-repeat center center;
    opacity: .8;
    font-size: 25px;
    background: #fcc90e;
}
.close:hover {
  opacity: 1;
}
@media (min-width: 767px) {
  .videoBox {
    width: 50%;
  }
}
</style>
<script>
$(function() {
  // CLOSE AND REMOVE ON ESC
  $(document).on('keyup',function(e) {
    if (e.keyCode == 27) {
      $('.overlay').remove();
    }
  });
  
  // CLOSE AND REMOVE ON CLICK
  $('body').on('click','.overlay, .close', function() {
    $('.overlay').remove();
  });
  
  // SO PLAYING WITH THE VIDEO CONTROLS DOES NOT
  // CLOSE THE POPUP
  $('body').on('click','.videoBox', function(e) {
    e.stopPropagation();
  });
});
</script>
<!----gallary----->
</body>

</html>