<?php
class Templates extends MX_Controller
{

function __construct() {
parent::__construct();
}

function test()
{
	$this->load->module('site_security');
	$this->site_security->_make_sure_is_admin();
	$data = "";
	$this->admin($data);
}

//Login Page
function login($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}
	$this->load->view('login', $data);
}

//Login Page
function login_user($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}
	$this->load->view('login_page', $data);
}

//Public Page
function public_bootstrap($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}

	$this->load->module('site_security');
	$data['user_id'] = $this->site_security->_get_user_id();

	$this->load->view('public_bootstrap', $data);
}

function conference_bootstrap($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}

	$this->load->module('site_security');
	$data['user_id'] = $this->site_security->_get_user_id();

	$this->load->view('conference_bootstrap', $data);
}

//Admin Page
function admin($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}
	$this->load->view('admin', $data);
}

//Admin Default Page
function admin_default($data)
{
	if (!isset($data['view_module'])) {
		# code...
		$data['view_module'] = $this->uri->segment(1);
	}
	$this->load->view('admin_default', $data);
}

//


public function Home()
{
	//SEO
	$data['meta_title'] = "MOC | Home";
	$data['meta_description'] = "In India, it is estimated and reported by government and various independent cancer";
	$data['meta_keywords'] = "";
	//Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];


	$data['view_file'] = "home";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}


}
