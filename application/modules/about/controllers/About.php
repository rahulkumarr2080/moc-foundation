<?php
class About extends MX_Controller
{

function __construct() {
parent::__construct();
}


function Donors()
{
    //SEO
	$data['meta_title'] = "MOC | Our Donors";
	$data['meta_description'] = "Associate Editor of Indian Journal of Social, Preventive and Rehabilitative Oncology.";
	$data['meta_keywords'] = "";
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "about";
    $data['view_file'] = "donors";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}


function About_us()
{
    //SEO
	$data['meta_title'] = "MOC | About Us";
	$data['meta_description'] = "Associate Editor of Indian Journal of Social, Preventive and Rehabilitative Oncology.";
	$data['meta_keywords'] = "";
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "about";
    $data['view_file'] = "about_us";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}


function Trustees()
{
    //SEO
    $data['meta_title'] = "MOC | Our trustees";
    $data['meta_description'] = "Associate Editor of Indian Journal of Social, Preventive and Rehabilitative Oncology.";
    $data['meta_keywords'] = "";
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "about";
    $data['view_file'] = "trustees";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}


}
