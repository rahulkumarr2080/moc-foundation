<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Our Trustees</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a>
                </li>
                <li><a href="index-2.html">About</a>
                </li>
                <li class="active">Our Trustees</li>
            </ul>
        </div>
    </div>
</section>
<!--End Banner Section -->
<section style="padding-top: 50px; padding-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 image-box2">
                <figure class="image-box1">
                    <img src="<?= base_url();?>assets/images/home/ashish-joshi.jpg">
                </figure>
                <h4 class="icon-headd">Dr. Ashish Joshi</h4>
            </div>
            <div class="col-md-3 image-box2">
                <figure class="image-box1">
                    <img src="<?= base_url();?>assets/images/home/kshitij-joshi.jpg">
                </figure>
                <h4 class="icon-headd">Dr. Kshitij Joshi</h4>
            </div>
            <div class="col-md-3 image-box2">
                <figure class="image-box1">
                    <img src="<?= base_url();?>assets/images/home/pritam-kalaskar.jpg">
                </figure>
                <h4 class="icon-headd">Dr. Pritam Kalaskar</h4>
            </div>
            <div class="col-md-3 image-box2">
                <figure class="image-box1">
                    <img src="<?= base_url();?>assets/images/home/vashistha-pankaj.jpg">
                </figure>
                <h4 class="icon-headd">Dr. Vashishth Maniar</h4>
            </div>
        </div>
    </div>
</section>