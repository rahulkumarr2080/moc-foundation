<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>About Us</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a>
                </li>
                <li><a href="index-2.html">About</a>
                </li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
</section>
<section style="padding-top: 50px; padding-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="main-headh2">MOC Cancer Care Foundation</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 text-justify">
                <p>Mumbai Oncocare Centre in the wake of its corporate social responsibility has created MOC Cancer Care Foundation with an intention to help economically backward silent sufferers with monetary / financial & otherwise assistance.</p>
                <p>MOC Cancer Care Foundation was instituted in September 2019 by Cellcure Cancer Centre Pvt. Ltd., a parent registered company of Mumbai Oncocare Centre. MOC Cancer Care Foundation has Dr. Vashishth Maniar being designated as ‘a Settlor’, at the helm of affairs, followed by other trustees, namely; Dr. Pritam Kalaskar, Dr. Ashish Joshi & Dr. Kshitij Joshi. All are qualified medical oncologists desirous of carrying out charitable objects and purposes, wide enough to confer benefits thereof on all persons irrespective of their caste, creed & community. They intend to work for relief of poor / economically backward class of people / patients / relatives of patients / families, primarily citizens of India with monetary / financial and otherwise benefits / help / assistance so that they can complete their cancer treatment and get well.</p>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-md-7 text-justify">
                <h3 class="main-headh3">Why MOC Cancer Care Foundation?</h3>
                <p>In India, it is estimated and reported by the government of India and various independent cancer registries of India that cancer incidences are rapidly growing due to increasing urbanisation, rapid adoption of western lifestyle as well as rampant, atrocious addictions like tobacco in Indian population. Among those who suffer from cancer, it is estimated that 10%-15% of their families are pushed below the poverty line due to the catastrophic cost burden of cancer treatment. Cancer treatment is primarily multi-modal in nature and each of its treatment modalities like Chemotherapy, Surgery, Radiotherapy, ranging up to latest inventions of Immunotherapy incur humongous cost to patients. On many occasions, patients are left with no choice but to interrupt treatment in-between. For some of them, cancer treatment remains a forbidden fruit. </p>
            </div>
            <div class="col-md-5">
                <img src="<?= base_url();?>assets/images/about/about.jpg" alt="">
            </div>
            <div class="col-md-12">
                <p>Mumbai Oncocare Centre realised that there is a need of a rescue mechanism that can help such patients and their families with financial and otherwise assistance, based on their eligibility so that they can access and complete their treatment, get well, live a reasonably healthy and quality life. </p>
                <p>MOC in the wake of our Corporate Social Responsibility instituted MOC Cancer Care Foundation that intends to help such underprivileged cancer patients.</p>
            </div>
        </div>
    </div>
</section>