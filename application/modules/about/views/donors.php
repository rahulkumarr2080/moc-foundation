<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Our Donors</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="index-2.html">Home</a>
                </li>
                <li><a href="index-2.html">About</a>
                </li>
                <li class="active">Our Donors</li>
            </ul>
        </div>
    </div>
</section>
<!--End Banner Section -->
<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <div class="pic">
                    <img src="<?= base_url();?>assets/images/man.png">
                </div>
                <div class="team-content">
                    <h3 class="title">Mr. XYZ</h3>
                    <span class="post">Donor</span>
                </div>
            </div>
        </div>
    </div>
</div>