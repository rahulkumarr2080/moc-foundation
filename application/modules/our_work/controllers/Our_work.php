<?php
class Our_work extends MX_Controller
{

function __construct() {
parent::__construct();
}


function Guidelines()
{
    //SEO
	$data['meta_title'] = "MOC | Our Donors";
	$data['meta_description'] = "Associate Editor of Indian Journal of Social, Preventive and Rehabilitative Oncology.";
	$data['meta_keywords'] = "";
	//Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];


    $data['view_module'] = "our_work";
    $data['view_file'] = "guidelines";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

}
