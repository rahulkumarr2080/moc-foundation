<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Guidelines MOC</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li><a href="#">About</a>
                </li>
                <li class="active">Guidelines</li>
            </ul>
        </div>
    </div>
</section>
<section style="padding-top: 50px; padding-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="main-headh3">Eligibility Criteria For Receiving Donations</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 text-justify">
                <ul class="guidelines-icon">
                    <li><span class="fa fa-arrow-right"></span> Trustees of MOC cancer care foundation reserve right to select or reject application for monetary assistance for cancer treatment by patients.</li>
                    <li><span class="fa fa-arrow-right"></span> Selection of patients for monetary assistance, in full or part, will be based on pre-defined criteria of the trust.</li>
                    <li><span class="fa fa-arrow-right"></span> Consideration of special cases at the cost of bypassing predefined criteria will be at the sole discretion of the trustees.</li>
                    <li><span class="fa fa-arrow-right"></span> Quantum of amount sanctioned will be at the sole discretion of trustees or authority appointed by them.</li>
                    <li><span class="fa fa-arrow-right"></span> MOC cancer care foundation under no circumstances will give / transfer the sanctioned amount to patient / personal accounts of patients / relatives directly.</li>
                    <li><span class="fa fa-arrow-right"></span> Sanctioned amount will be utilized in parts for the treatment of patients.</li>
                    <li><span class="fa fa-arrow-right"></span> Surplus / left over money if any, post treatment of any patient will be credited back to MOC foundation.</li>
                    <li><span class="fa fa-arrow-right"></span> Patient will have no rights to claim left over money of the said sanctioned amount.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
