<?php
class Timedate extends MX_Controller
{

function __construct() {
parent::__construct();
}

function get_nice_date($timestamp, $format)
{
    switch ($format){
        case 'cool':
        $the_date = date('l jS \of F Y', $timestamp);
        break;
        case 'mini':
        $the_date = date('jS M Y',$timestamp);
        break;
    }
    return $the_date;
}

}
