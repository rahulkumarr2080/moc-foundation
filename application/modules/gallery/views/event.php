<!-- Page Banner Section -->
<section class="page-banner">
  <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
  <div class="auto-container">
    <h1>Gallery</h1>
  </div>
  <div class="breadcrumb-box">
    <div class="auto-container">
      <ul class="bread-crumb clearfix">
        <li><a href="<?= base_url();?>">Home</a>
        </li>
        <li class="active">Gallery</li>
      </ul>
    </div>
  </div>
</section>
<!-- button start-->
<div class="container text-center pt-5">
  <!-- <a href="<?= base_url();?>gallery/event">
    <?php echo form_submit([ 'name'=>'submit','value'=>'All','class'=>'btn btn-muted actbtn']); ?></a> -->
</div>
<!-- button end -->
<section style="padding-top: 30px;padding-bottom: 30px;">
    <div class="sponsors-outer">
        <div class="auto-container">
            <!--Sponsors Carousel-->
            <div class="kausid-carousel owl-theme owl-carousel" data-options='{"loop": true, "margin": 40, "autoheight":true, "lazyload":true, "nav": true, "dots": true, "autoplay": true, "autoplayTimeout": 6000, "smartSpeed": 300, "responsive":{ "0" :{ "items": "1" }, "600" :{ "items" : "2" }, "768" :{ "items" : "3" } , "800":{ "items" : "3" }, "1024":{ "items" : "4" }}}'>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/woman-cancer-gallery"><img src="<?= base_url();?>assets/images/gallery/all/1.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Awareness on Women's Cancers </h4>
                    <p class="gallery-para">Campaign conducted by Dr. Udip Maheshwari. It was delightful experience addressing and interacting with a gathering of over 300 females.</p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cervical-cancer-gallery"><img src="<?= base_url();?>assets/images/gallery/all/2.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">PAP smear test for cervical cancer detection </h4>
                    <p class="gallery-para">Dr. Pritam Kalaskar and Dr. Smit Sheth has successfully screened over 60 females at MOC, Thane.</p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/mammography-camp-camp-gallery"><img src="<?= base_url();?>assets/images/gallery/all/3.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Mammography Camp at Camp </h4>
                    <p class="gallery-para">We got overwhelming response to Mammography Camp held by  Dr. Udip Maheshwari  at Dombivali.</p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/breast-cancer-awareness-gallery"><img src="<?= base_url();?>assets/images/gallery/all/4.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Breast Cancer awareness program</h4>
                    <p class="gallery-para">Dr. Pritam Kalaskar conducted Breast Cancer awareness program on an occasion of Breast Cancer awareness month. </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/mammography-camp-chetna-college-gallery"><img src="<?= base_url();?>assets/images/gallery/all/5.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Mammography Camp at Chetna College</h4>
                    <p class="gallery-para">Dr. Kshitij Joshi  has conducted Mammography Camp. We got overwhelming response to our Mammography Camp.
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cervical-cancer-camp-gallery"><img src="<?= base_url();?>assets/images/gallery/all/6.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Cervical cancer detection camp </h4>
                    <p class="gallery-para">Cervical Cancer detection camp at Lions Club, Santacruz was conducted by Dr. Kshitij Joshi.
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/mammography-camp-vile-parle-gallery"><img src="<?= base_url();?>assets/images/gallery/all/7.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Mammography Camp at Vile Parle</h4>
                    <p class="gallery-para">Overwhelming response to the Mammography Camp for Breast Cancer screening conducted Dr. Kshitij Joshi.
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/free-mother-cancer-gallery"><img src="<?= base_url();?>assets/images/gallery/all/8.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">"Cancer Free Mother" at Shirdi</h4>
                    <p class="gallery-para">It was huge gathering of 1400 girls and women put together. We delighted to address participants on Breast, Ovarian and Cervical cancer awareness. 
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/awareness-program-gallery"><img src="<?= base_url();?>assets/images/gallery/all/9.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Awareness Program at Dadar </h4>
                    <p class="gallery-para">Dr. Udip Maheshwari conducted  Breast Cancer awareness camp with a large gathering of over 300 females.
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cancer-screening-camp-vile-parle-gallery"><img src="<?= base_url();?>assets/images/gallery/all/10.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Cancer screening camp at Vile Parle </h4>
                    <p class="gallery-para">Dr. Ashish Joshi & Dr. Bela Jagtap has successfully conducted cancer screening camp with 100 females in the region.
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cancer-awareness-shipping-company-gallery"><img src="<?= base_url();?>assets/images/gallery/all/11.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Cancer Awareness at  shipping company </h4>
                    <p class="gallery-para">Dr. Kshitij Joshi has given talk on Cancer Awareness at a shipping company. Great Initiative by MOC!
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/breast-and-cervical-cancer-awareness-gallery"><img src="<?= base_url();?>assets/images/gallery/all/12.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Women's Day - Breast and Cervical cancer awareness  at TATA AIG insurance </h4>
                    <p class="gallery-para">On occasion of Women's Day Dr. Pradip Kendre has given talk on Breast and Cervical Cancer awareness at TATA AIG insurance, Goregaon. 
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cancer-awareness-talk-mulund-gallery"><img src="<?= base_url();?>assets/images/gallery/all/13.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">cancer awareness talk at Mulund Bunts Samaj</h4>
                    <p class="gallery-para">We are  happy to enlighten people of Mulund Bunts Samaj with cancer awareness talk. An initiative by Dr. Pritam Kalaskar, MOC, Thane. 
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cancer-free-mother-mukta-aai-gallery"><img src="<?= base_url();?>assets/images/gallery/all/14.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">"Cancer Free Mother" (Cancer Mukta Aai)</h4>
                    <p class="gallery-para">Another small yet important step to help our mothers in Maharashtra to prevent themselves from cancer. An initiative by Dr. Pradip Kendre. 
                    </p>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><a href="<?= base_url();?>gallery/cancer-awareness-talk-hdfc-life-gallery"><img src="<?= base_url();?>assets/images/gallery/all/15.jpg" alt=""></a>
                    </figure>
                    <h4 class="gallery-heading">Cancer awareness talk at HDFC Life</h4>
                    <p class="gallery-para">Happy to enlighten on cancer awareness and address queries of HDFC Life employees.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>