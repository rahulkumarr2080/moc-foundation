<!-- Page Banner Section -->
<section class="page-banner">
  <div class="image-layer" style="background-image: url(../assets/images/background/bg-page-title-2.jpg);"></div>
  <div class="auto-container">
    <h1>Breast Cancer awareness program</h1>
  </div>
  <div class="breadcrumb-box">
    <div class="auto-container">
      <ul class="bread-crumb clearfix">
        <li><a href="<?= base_url();?>">Home</a>
        </li>
        <li><a href="<?= base_url();?>gallery/event">Gallery</a>
        </li>
        <li class="active">Breast Cancer awareness program</li>
      </ul>
    </div>
  </div>
</section>
<!-- button start-->
<div class="container text-center pt-5">
  <a href="<?= base_url();?>gallery/event">
    <?php echo form_submit([ 'name'=>'submit','value'=>'All','class'=>'btn btn-muted actbtn']); ?></a>
</div>
<!-- button end -->
<!-- Gallery Page Container -->

<div class="row">
  <div class="container gallery-container">
    <div class="tz-gallery">
      <!--row 1 open-->
      <div class="row">
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/1.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/1.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/2.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/2.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/3.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/3.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
      </div>
      <!---row 1 close--->
      <!--row 1 open-->
      <div class="row">
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/4.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/4.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/5.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/5.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/6.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/6.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
      </div>
      <!---row 1 close--->
      <!--row 1 open-->
      <div class="row">
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/7.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/7.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/8.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/8.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/9.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/9.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
      </div>
      <!---row 1 close--->
      <!--row 1 open-->
      <div class="row">
        <div class=" col-md-4 col-sm-6">
          <a class="lightbox" href="<?= base_url();?>assets/images/gallery/4/10.jpg">
          <img src="<?= base_url();?>assets/images/gallery/4/10.jpg" alt="img" class="img-fluid about" style=" border-radius:8px;">
          </a>
        </div>
      </div>
      <!---row 1 close--->
      <!------->
    </div>
  </div>
</div>