<?php
class Gallery extends MX_Controller
{

function __construct() {
parent::__construct();
}

public function event()
{

	$data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

  $data['view_module'] = "gallery";
  $data['view_file'] = "event";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function woman_cancer_gallery()
{

	$data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

  $data['view_module'] = "gallery";
  $data['view_file'] = "woman_cancer_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cervical_cancer_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cervical_cancer_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cervical_cancer_camp_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cervical_cancer_camp_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function free_mother_cancer_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "free_mother_cancer_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function awareness_program_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "awareness_program_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function mammography_camp_camp_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "mammography_camp_camp_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function breast_cancer_awareness_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "breast_cancer_awareness_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function mammography_camp_chetna_college_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "mammography_camp_chetna_college_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function mammography_camp_vile_parle_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "mammography_camp_vile_parle_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cancer_screening_camp_vile_parle_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cancer_screening_camp_vile_parle_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cancer_awareness_shipping_company_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cancer_awareness_shipping_company_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function breast_and_cervical_cancer_awareness_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "breast_and_cervical_cancer_awareness_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cancer_awareness_talk_mulund_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cancer_awareness_talk_mulund_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cancer_free_mother_mukta_aai_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cancer_free_mother_mukta_aai_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

public function cancer_awareness_talk_hdfc_life_gallery()
{

    $data['meta_title'] = 'MOC | Gallery';
	$data['meta_description'] = 'Forget Password';
	$data['meta_keywords'] = '';
	//Facebook Open Graph
	$data['meta_ogimage'] = "";
	$data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
	        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
	        $_SERVER['REQUEST_URI'];

	        
  $data['view_module'] = "gallery";
  $data['view_file'] = "cancer_awareness_talk_hdfc_life_gallery";
  $this->load->module('templates');
  $this->templates->public_bootstrap($data);
}

}

