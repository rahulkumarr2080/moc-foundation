<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_User extends CI_Model
{

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "tablename";
    return $table;
}

public function login_valid($username, $password)
{

  $q = $this->db->where(['userid'=>$username,'password'=>$password])
                ->get('login_users');
    

    if($q->num_rows())
    {
        
        return $q->row()->userid;
    }
    
    else
    {
        
        return false;
    }
    
}

public function register_user($registrationDetails)
{
    $this->db->insert('user',['loginid'=>rand(1111111,9999999),'fname'=>$registrationDetails['firstname'],'lname'=>$registrationDetails['lastname'],'email'=>$registrationDetails['useremail'],'password'=>password_hash($registrationDetails['password'],PASSWORD_DEFAULT),'contact'=>$registrationDetails['contact'],'address1'=>$registrationDetails['address1'],'address2'=>$registrationDetails['address2'],'zipcode'=>$registrationDetails['zipcode'],'city'=>$registrationDetails['city'],'landmark'=>$registrationDetails['landmark'],'state'=>$registrationDetails['state'],'date'=>time()]);
}

public function check_username($loginDetails)
{
    $email  = $loginDetails['useremail'];
    $sql = "SELECT * FROM `user` WHERE email = '$email'";
            $result = $this->db->query($sql);
            $row = $result->row();
            if($row)
            {
                return true;
            }
            else
            {
                return false;
            }
}
public function check_password($loginDetails)
{
    $email  = $loginDetails['useremail'];
    $password  = $loginDetails['password'];
    $sql = "SELECT * FROM `user` WHERE email = '$email'";
                $result = $this->db->query($sql);
                $row = $result->row();
                $counter = password_verify($password,$row->password);
                if($counter == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
}

public function update_forget_link_of_user($id,$rand)
{
    $this->db->set('forgetid', $rand); //value that used to update column  
    $this->db->where('id', $id); //which row want to upgrade  
    $this->db->update('user');  //table name    
}

public function get_id($loginDetails)
{
    $email = $loginDetails['useremail'];
    $sql = "SELECT * FROM `user` WHERE email = '$email' AND status = 1";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->id;
}
public function check_token($id, $rand)
{
     $sql = "SELECT * FROM `user` WHERE id = '$id' AND forgetid = '$rand'";
            $result = $this->db->query($sql);
            $row = $result->row();
            if($row)
            {
                return true;
            }
            else
            {
                return false;
            }
}

public function reset_password($resetPasswordDetails,$id)
{
    $this->db->set('password', password_hash($resetPasswordDetails['new_password'],PASSWORD_DEFAULT)); //value that used to update column  
    $this->db->where('id', $id); //which row want to upgrade  
    $this->db->update('user');  //table name       
}

function get_name($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->fname." ".$row->lname;
}

function get_address($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->address1;
}

function get_city($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->city;
}

function get_zip($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->zipcode;
}

function get_contact($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->contact;
}

function get_email($userid)
{
    $sql = "SELECT * FROM `user` WHERE id =  '$userid'";
    $result = $this->db->query($sql);
    $row = $result->row();
    return $row->email;
}

}
