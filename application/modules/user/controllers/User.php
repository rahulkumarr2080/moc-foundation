<?php
class User extends MX_Controller 
{

function __construct() {
parent::__construct();
}

// public function index()
// {
//     //SEO
//     $data['meta_title'] = 'MOC | Login';
//     $data['meta_description'] = 'Login.';
//     $data['meta_keywords'] = '';

//     $data['view_module'] = "user";
//     $data['view_file'] = "login";
//     $this->load->module('templates');
//     $this->templates->public_bootstrap($data);
// }

public function login()
{
    $data['meta_title'] = 'MOC | Login';
    $data['meta_description'] = 'Login.';
    $data['meta_keywords'] = '';
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "user";
    $data['view_file'] = "login";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submit()
{      
      $this->load->model('Mdl_User');
       $this->form_validation->set_rules('useremail','Email address','required|trim');
       $this->form_validation->set_rules('password','password','required|trim');
       
       if($this->form_validation->run())
       { 
          $loginDetails = $this->input->post();
          $checkUsername = $this->Mdl_User->check_username($loginDetails);
          if($checkUsername)
          {
              $checkPassword = $this->Mdl_User->check_password($loginDetails);
              if($checkPassword)
              {
                $this->session->set_userdata('user_id', $this->Mdl_User->get_id($loginDetails));
                $sessionData = $this->session->get_userdata();
                //$userId = $sessionData['user_id'];
                //echo($userId); die();
                $data['meta_title'] = 'MOC | Dashboard';
                $data['meta_description'] = 'Dashboard';
                $data['meta_keywords'] = '';
                //Facebook Open Graph
                $data['meta_ogimage'] = "";
                $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
                        $_SERVER['REQUEST_URI'];

                $data['view_module'] = "user";
                $data['view_file'] = "dashboard";
                $this->load->module('templates');
                $this->templates->public_bootstrap($data);
              }
              else
              {
                $this->session->set_flashdata('flash_message', '<p class="alert alert-danger text-center">'.'You have entered Wrong Username or Password, Please enter valid credentials !!'.'</p>');
                redirect('user/login');     
              }
          }
          else
          {
            $this->session->set_flashdata('flash_message', '<p class="alert alert-danger text-center">'.'You have entered Wrong Username or Password, Please enter valid credentials !!'.'</p>');
            redirect('user/login');
          }
          //print_r($loginDetails);
       }
        else 
        {
          $this->login();    
       }
}

public function register()
{
   //SEO
    $data['meta_title'] = 'MOC | New User Registration';
    $data['meta_description'] = 'User Registration.';
    $data['meta_keywords'] = '';
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "user";
    $data['view_file'] = "register";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submitregister()
{
  $this->load->model('Mdl_User');
  $this->form_validation->set_rules('firstname','First Name','required|alpha',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'alpha' => '%s should contain only alphabates.'
                        )
                    );
  $this->form_validation->set_rules('lastname','Last Name','required|alpha',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'alpha' => '%s should contain only alphabates.'
                        )
                    );
  $this->form_validation->set_rules('useremail','Email Address','required|valid_email|is_unique[user.email]',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'is_unique' => '%s is already exist please enter new Email Address.',
                        'valid_email' => '%s you have entered is not valid.'
                        )
                    );
   $this->form_validation->set_rules('password','Password','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
   $this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[password]', 
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'matches' => 'Password and %s does not match.'
                        )
                    );
   $this->form_validation->set_rules('contact','Mobile Number','required|is_unique[user.contact]|numeric',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'is_unique' => '%s is already exist please enter new Mobile Number.',
                        'numeric' => '%s should not contain text.'
                        )
                    );
   $this->form_validation->set_rules('address1','Address','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
   $this->form_validation->set_rules('zipcode','Zipcode/Pincode','required|numeric',
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'numeric' => '%s should not contain text.'
                        )
                    );
   $this->form_validation->set_rules('city','City','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
   $this->form_validation->set_rules('state','State','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
  if($this->form_validation->run())
  {
    $registrationDetails = $this->input->post();
    //print_r($registrationDetails); exit();
    $this->Mdl_User->register_user($registrationDetails);
    //$this->send_confirmation_mail_to_user($registrationDetails);
    //$this->send_confirmation_mail_to_admin($registrationDetails);
    $this->session->set_flashdata('flash_message', '<p class="alert alert-success text-center">'.'Thanks for registration for more details Please Check your Email Address. !!'.'</p>');
    redirect('user/register');
  }
  else
  {
    $this->register();
  }

  //echo "string";
}

public function forgetpassword()
{
    $data['meta_title'] = 'MOC | Forget Password';
    $data['meta_description'] = 'Forget Password';
    $data['meta_keywords'] = '';
    //Facebook Open Graph
    $data['meta_ogimage'] = "";
    $data['link'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
            "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
            $_SERVER['REQUEST_URI'];

    $data['view_module'] = "user";
    $data['view_file'] = "forgetpassword";
    $this->load->module('templates');
    $this->templates->public_bootstrap($data);
}

public function submitemail()
{
  $this->load->model('Mdl_User');
  $this->form_validation->set_rules('useremail','Email Address','required|trim');
  if($this->form_validation->run())
  {
    $loginDetails = $this->input->post();
    $checkUsername = $this->Mdl_User->check_username($loginDetails);
    if($checkUsername)
    {
      $rand = rand(111111111,999999999);
      $id = $this->Mdl_User->get_id($loginDetails);
      //print_r($id); exit();
      $this->Mdl_User->update_forget_link_of_user($id,$rand);
      $this->send_reset_password_mail_to_user($loginDetails,$id,$rand);
      $this->session->set_flashdata('flash_message', '<p class="alert alert-success text-center">'.'We have sent you Reset Password Link to your email address. !!'.'</p>');
      redirect('user/forgetpassword');
    }
    else
    {
      $this->session->set_flashdata('flash_message', '<p class="alert alert-danger text-center">'.'You have entered Invalid Email Address please enter Valid Email Address. !!'.'</p>');
      redirect('user/forgetpassword'); 
    }
  }
  else
  {
      $this->forgetpassword();
  }
}

public function resetpassword()
{
    $this->load->model('Mdl_User'); 
    $id =  $this->uri->segment(3)/124;
    $rand =  $this->uri->segment(4)/124;
    $checkToken = $this->Mdl_User->check_token($id, $rand);
    $data['meta_title'] = 'MOC | Reset Password';
    $data['meta_description'] = 'Reset Password';
    $data['meta_keywords'] = '';
    $data['view_module'] = "user";
    if($checkToken)
    {
      $data['view_file'] = "resetpassword";
    }
    else
    {
      $data['view_file'] = "tokenExpired";
    }
    $this->load->module('templates');
    $this->templates->public_bootstrap($data); 
}

public function submitresetpassword()
{
  $this->load->model('Mdl_User');
  //echo "string"; die();
  $id =  $this->uri->segment(3);
  $this->form_validation->set_rules('new_password','New Password','required',
                      array(
                        'required' => 'Please Enter %s to continue.'
                        )
                    );
   $this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[new_password]', 
                      array(
                        'required' => 'Please Enter %s to continue.',
                        'matches' => 'Password and %s does not match.'
                        )
                    );
  if($this->form_validation->run())
  {
    $resetPasswordDetails = $this->input->post();
    $this->Mdl_User->reset_password($resetPasswordDetails,$id);
    $rand = 0;
    $this->Mdl_User->update_forget_link_of_user($id,$rand);
    $this->session->set_flashdata('flash_message', '<p class="alert alert-success text-success">'.'Your Password Reset Successfully. !!'.'</p>');
      redirect('user/login'); 
  }
  else
  {
    $this->resetpassword();
  }
}
function logout()
{
  unset($_SESSION['user_id']);
  $this->load->module('site_cookies');
  $this->site_cookies->_destroy_cookie();
  redirect(base_url());
}
public function send_confirmation_mail_to_user($registrationDetails)
{
  $this->load->library('email');

    $Useremail = $email;
    
    $name = $Fullname;
    $MSG = 'Hi';

        
      

      $subject = 'New Registration from - ';
      $message = '
      <center>
    <div style="width: 700px; background-color: #fefcf1;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
      <img src="http://localhost/moc/assets/images/logo.png" alt="Cancer Care Foundation Logo" style=" display: block; margin-left: auto; margin-right: auto;">
    </div>
      <div style="width: 700px; background-color: #f7f7f7;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
      <p style="font-size: 17px; font-weight: 600; text-align: justify; margin-top: 0px; padding-top:  15px; ">Dear User,</p>
      <p style="font-size: 17px; text-align: justify;">Greetings from <a href="http://localhost/moc/" style="text-decoration: none; font-weight: 800; color: #e05b5e;" target="_blank">Cancer Care Foundation</a></p>
      <p style="font-size: 17px; text-align: justify;">Thank you for registration.</p>
    </div>
  </center>

      ';

      // Get full html:
      $body = '<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>New User Registration</title>
</head>
<body>
          ' . $message . '
        
  </body>
</html>';


        $result = $this->email
        ->from('info@oncologyforum.org', 'Cancer Care Foundation')
        ->reply_to('info@oncologyforum.org')    // Optional, an account where a human being reads.
        ->to($Useremail)
        ->subject($subject)
        ->message($body)
        ->send();
}

public function send_confirmation_mail_to_admin($registrationDetails)
{
  $this->load->library('email');

    $Useremail = $email;
    
    $name = $Fullname;
    $MSG = 'Hi';

        
      

      $subject = 'New Registration from - ';
      $message = '
      <center>
    <img src="http://localhost/moc/assets/images/logo.png" alt="Cancer Care Foundation Logo" style=" display: block; margin-left: auto; margin-right: auto;">
    <div style="width: 700px; background-color: #411e75 ;  padding-left: 10px; padding-top: 10px; padding-bottom:  10px; padding-right: 10px; border-radius: 5px; text-align: center; margin-top: 10px; color: #fff;">
      <h3>New User Registration</h3>
    </div>
      <div style="width: 700px; background-color: #f7f7f7;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
      <p style="font-size: 17px; font-weight: 600; text-align: justify; margin-top: 0px; padding-top:  15px; ">Dear "Name Of Receiver",</p>
      <p style="font-size: 17px; text-align: justify;">Greetings from <a href="http://localhost/moc/" style="text-decoration: none; font-weight: 800; color: #e05b5e;" target="_blank">Cancer Care Foundation</a></p>
      <p style="font-size: 17px; text-align: justify;">There is a new user Registration , details of User are given as below,</p>
      <table id="customers" style=" border-collapse: collapse; width: 85%;">
      <tr>
          <th class="header-th" colspan="2" style="text-align: center;  padding-top: 12px; padding-bottom: 12px; text-align: center; background-color: #411e75; color: #fff;">Personal Details</th>
       </tr>
        <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Name</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
        <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Email</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
         <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Contact Number</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
         <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Address</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
         <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">City</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
         <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">State</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
         <tr>
          <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; text-align: center; border: 1px solid #ddd; padding: 8px;">Zipcode</th>
          <td style="border: 1px solid #ddd; padding: 8px;">Maria Anders</td>
        </tr>
      </table>
      <p style="font-size: 17px; text-align: justify; margin-top: 10px; padding-bottom: 15px;">Thank you.</p>
    </div>
  </center>
      ';

      // Get full html:
      $body = '
      <!DOCTYPE html>
<html>
<head>
  <title>Cancer Care Foundation New User Registration</title>
  <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
  <style>
  body {
    font-family: "Raleway", sans-serif;
  }   

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}
</style>
</head>
<body>
      ';


        $result = $this->email
        ->from('info@oncologyforum.org', 'Cancer Care Foundation')
        ->reply_to('info@oncologyforum.org')    // Optional, an account where a human being reads.
        ->to($Useremail)
        ->subject($subject)
        ->message($body)
        ->send();
}

public function send_reset_password_mail_to_user($loginDetails,$id,$rand)
{
   $this->load->library('email');
    $Useremail = $loginDetails['useremail'];
    //$name = $Fullname;
    //$MSG = 'Hi';
      $subject = 'Forget Password Link';
      $message = '
        <center>
    <div style="width: 700px; background-color: #fefcf1;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
      <img src="http://rrcgvir.com/moc/assets/images/logo.png" alt="Cancer Care Foundation Logo" style=" display: block; margin-left: auto; margin-right: auto;">
    </div>
      <div style="width: 700px; background-color: #f7f7f7;  padding-left: 10px; padding-right: 10px; border-radius: 5px;">
      <p style="font-size: 17px; font-weight: 600; text-align: justify; margin-top: 0px; padding-top:  15px; ">Dear User,</p>
      <p style="font-size: 17px; text-align: justify;">Greetings from <a href="http://rrcgvir.com/moc/" style="text-decoration: none; font-weight: 800; color: #e05b5e;" target="_blank">Cancer Care Foundation</a></p>
      <p style="font-size: 17px; text-align: justify; padding-bottom: 10px;">To reset your password please <a href="http://rrcgvir.com/moc/user/resetpassword/'.($id * 124).'/'.($rand * 124).'" style="text-decoration: none; font-weight: 800; color: #e05b5e;" target="_blank">Click Here.</a></p>
      <p style="font-size: 17px; text-align: justify; padding-bottom: 10px;">Thank you.</p>
    </div>
  </center>
      ';

      // Get full html:
      $body = '<!DOCTYPE html>
<html>
<head>
  <title>Reset Password Link</title>
</head>
<body>
          ' . $message . '
        
  </body>
</html>';


        $result = $this->email
        ->from('info@oncologyforum.org', 'Cancer Care Foundation')
        ->reply_to('info@oncologyforum.org')    // Optional, an account where a human being reads.
        ->to($Useremail)
        ->subject($subject)
        ->message($body)
        ->send();

        echo $body; die();
}

}