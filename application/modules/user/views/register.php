<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>User Registration</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li class="active">User Registration</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <?php if ($this->session->flashdata('flash_message')): ?>
            <?php echo $this->session->flashdata('flash_message'); ?>
            <?php endif; ?>
            <!--Form Column--->
            <div class="form-column offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12" style="background-color: #fefcf1;">
               <h1 style="font-size: 30px; padding: 20px;">Register Here</h1>
                <hr>
                <div class="inner" style="padding: 30px;">
                    <div class="default-form contact-form">
                        <form method="post" action="<?= base_url();?>user/submitregister" id="contact-form">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="firstname"><strong>First Name:</strong></label></div>
                                    <input type="text" name="firstname" id="firstname" placeholder="First Name" value="<?php echo set_value('firstname'); ?>">
                                    <?php echo form_error('firstname', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="lastname"><strong>Last Name:</strong></label></div>
                                    <input type="text" id="lastname" name="lastname" placeholder="Last Name" value="<?php echo set_value('lastname'); ?>">
                                    <?php echo form_error('lastname', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="useremail"><strong>Email Address:</strong></label></div>
                                    <input type="text" id="useremail" name="useremail" placeholder="Email Address" value="<?php echo set_value('useremail'); ?>">
                                    <?php echo form_error('useremail', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="password"><strong>Password:</strong></label></div>
                                    <input type="password" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
                                    <?php echo form_error('password', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="confirm_password"><strong>Confirm Password:</strong></label></div>
                                    <input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" value="<?php echo set_value('confirm_password'); ?>">
                                    <?php echo form_error('confirm_password', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="contact"><strong>Mobile Number:</strong></label></div>
                                    <input type="text" id="contact" name="contact" placeholder="Mobile Number" value="<?php echo set_value('contact'); ?>">
                                    <?php echo form_error('contact', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><label for="address1"><strong>Address:</strong></label></div>
                                    <input type="text" id="address1" name="address1" placeholder="Address Line 1" value="<?php echo set_value('address1'); ?>">
                                    <?php echo form_error('address1', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                 <div class="col-md-12 col-sm-12 form-group">
                                    <input type="text" id="address2" name="address2" placeholder="Address Line 2 (optional)" value="<?php echo set_value('address2'); ?>">
                                </div>
                                 <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="zipcode"><strong>Zipcode/Pincode:</strong></label></div>
                                    <input type="text" name="zipcode" id="zipcode" placeholder="Zipcode" value="<?php echo set_value('zipcode'); ?>">
                                    <?php echo form_error('zipcode', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="city"><strong>City:</strong></label></div>
                                    <input type="text" id="city" name="city" placeholder="Enter City" value="<?php echo set_value('city'); ?>">
                                    <?php echo form_error('city', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="landmark"><strong>Landmark:</strong></label></div>
                                    <input type="text" name="landmark" id="landmark" placeholder="Landmark (optional)" value="<?php echo set_value('landmark'); ?>">
                                    <?php echo form_error('landmark', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 col-sm-12 form-group">
                                    <div class="field-label"><label for="state"><strong>State:</strong></label></div>
                                    <input type="text" id="state" name="state" placeholder="State" value="<?php echo set_value('state'); ?>">
                                    <?php echo form_error('state', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>

                                <div class="col-md-12 col-sm-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Create Account</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>