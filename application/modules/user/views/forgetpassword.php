<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Forget Password</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li class="active">Forget Password</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column--->
            <div class="form-column offset-xl-3 col-xl-7 col-lg-12 col-md-12 col-sm-12">
                <p class="text-center">Please Enter your Email Address to change your password.</p>
                <hr>
                <div class="inner">
                    <div class="default-form contact-form">
                        <form method="post" action="<?= base_url();?>user/submitemail" id="contact-form">
                            <?php if ($this->session->flashdata('flash_message')): ?>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                            <?php endif; ?>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group">
                                    <div class="field-label"><strong>Email Address:</strong></div>
                                    <input type="text" name="useremail" placeholder="Enter Email Address" value="<?php echo set_value('useremail'); ?>">
                                    <?php echo form_error('useremail', '<div class="text-danger text-left mb-2">', '</div>'); ?>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Submit</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>