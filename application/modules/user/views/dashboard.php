<!-- Page Banner Section -->
<section class="page-banner">
    <div class="image-layer" style="background-image: url(./assets/images/background/bg-page-title-2.jpg);"></div>
    <div class="auto-container">
        <h1>Dashboard</h1>
    </div>
    <div class="breadcrumb-box">
        <div class="auto-container">
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url();?>">Home</a>
                </li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
</section>
<!--Contact Section-->
<section class="contact-section contact-page">
    <div class="icon-one paroller" data-paroller-factor="-0.20" data-paroller-factor-lg="-0.20" data-paroller-factor-sm="-0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-two paroller" data-paroller-factor="0.20" data-paroller-factor-lg="0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-heart-2"></span>
    </div>
    <div class="icon-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.15" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal"><span class="flaticon-dove"></span>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column--->
            <div class="form-column offset-xl-3 col-xl-7 col-lg-12 col-md-12 col-sm-12">
                <p class="text-center">Payment Gateway</p>
                <h2>You have to Pay 2 Rupees.</h2>
                <hr>
                <div class="inner">
                    <div class="default-form contact-form">
                    </div>
                </div>
            </div>

        </div>
    </div>
     <?php 
    $transactionId = rand(11111,99999);
    $sessionData = $this->session->get_userdata();
    $userId = $sessionData['user_id'];
    $finalAmount = 2;
    $this->load->module('user');
    $this->load->model('Mdl_User');
  ?>
    <div class="container">
  <form action="<?php echo base_url(); ?>payment/request" method="POST" target="_blank">
    <input type="hidden" name="tid" id="tid" value="<?php echo $transactionId; ?>" />
    <input type="hidden" name="id" value="<?php echo $userId; ?>"/>
    <input type="hidden" name="merchant_id" value="191399"/>
    <input type="hidden" name="amount" value="<?php echo $finalAmount; ?>"/>
    <input type="hidden" name="currency" value="INR"/>
    <input type="hidden" name="redirect_url" value="<?php echo base_url();?>payment/response"/>
    <input type="hidden" name="cancel_url" value="<?php echo base_url();?>payment/response"/>
    <input type="hidden" name="language" value="EN"/>
    <input type="hidden" name="billing_name" value="<?php echo $this->Mdl_User->get_name($userId);?>"/>
    <input type="hidden" name="billing_address" value="<?php echo $this->Mdl_User->get_address($userId);?>"/>
    <input type="hidden" name="billing_city" value="<?php echo $this->Mdl_User->get_city($userId);?>"/>
    <input type="hidden" name="billing_state" value="MH"/>
    <input type="hidden" name="billing_zip" value="<?php echo $this->Mdl_User->get_zip($userId);?>"/>
    <input type="hidden" name="billing_country" value="India"/>
    <input type="hidden" name="billing_tel" value="<?php echo $this->Mdl_User->get_contact($userId);?>"/>
    <input type="hidden" name="billing_email" value="<?php echo $this->Mdl_User->get_email($userId);?>"/>
    
    <input type="text" style="visibility: hidden;">

    <!--<input type="submit" value="Proceed to CheckOut" class="button button-3d fright">-->
    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Proceed to CheckOut</span>
                                    </button>
</form>

</div>
</section>